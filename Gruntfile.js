'use strict';
module.exports = function(grunt) {
    grunt.initConfig({
        copyto: {
            localwebserver: {
                files: [
		    {
			cwd: '.',
			src: 'src/_js/require.js',
			dest: '_site/js',
		    },
		    {
                        cwd: '_site', 
                        src: ['**/*'], 
                        dest: '/Library/WebServer/Documents/projectzen/'
                }]
            }
        },
        jslint: {
            client: {
                src: 'src/js/lib/main.js', 
                directives: { 
                    browser: true,
                    predef: ['jQuery', '$', 'console', 'define', 'require']
                }
            }
        },
        watch: {
            site: { 
                files: [
                    'src/**/*',
                ],
                tasks: ['build'],
                options: {
                  
                }
            }
        },
        sass: { 
            dist: { 
                options: { style: 'expanded' },
                files: { 
                    '_site/css/style.css': 'src/_scss/style.scss',
                    '_site/css/style-wide.css': 'src/_scss/style-wide.scss',
                    '_site/css/style-narrow.css': 'src/_scss/style-narrow.scss',
                    '_site/css/style-mobile.css': 'src/_scss/style-mobile.scss',
                }
            }
        },
        jekyll: {
            options: {
                src: 'src/',
                watch: false,
                serve: false,
            },
            dist: { 
                dest: 'site/',
            }, 
        },
        requirejs: {
            compile: {
                options: { 
                    baseUrl: 'src/_js/',
		    mainConfigFile: 'src/_js/main.js',
		    dir: '_site/js',
		    modules: [{
			name: 'main',
			include: ['skel', 'jquery']
		    }]
                }
            }

        }
    });
    grunt.loadNpmTasks('grunt-contrib-requirejs');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-jslint');
    grunt.loadNpmTasks('grunt-jekyll');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-copy-to');
    grunt.registerTask('build', ['jekyll', 'sass', 'requirejs', 'copyto']);
    grunt.registerTask('default', ['build', 'watch']);
};
