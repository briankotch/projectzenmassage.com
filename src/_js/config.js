
window._skel_config = {
    prefix: "css/style",
    resetCSS: true,
    preloadStyleSheets: true,
    boxModel: "border",
    grid: { gutters: 0 },
    containers: "1200",
    breakpoints: {
        wide: { range: "961-", containers: "960" },
        narrow: { range: "481-960", containers: "fluid" },
        mobile: { range: "-480", containers: "fluid", lockViewport: true, grid: { collapse: true } }
    }
};
