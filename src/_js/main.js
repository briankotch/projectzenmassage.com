requirejs.config({ 
    baseurl: './js',
    paths: { 
	'jquery': 'vendor/jquery.min',
	'skel': 'vendor/skel.min',
	'config': 'config',
    },
    shim: { 
	'skel': ['config']
    }
});
